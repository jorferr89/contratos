@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="h1" align="center">
                Contracts
            </div>

            <div class="h3" align="center">Editar Objeto</div>

            <form class="card-body" method="post" action="{{route('objetos.actualizar', $objeto)}}">
            
                {{ csrf_field() }}
                {{ method_field('put') }}


                <div class="form-group">
                    <label for="nombre">
                        Descripción
                    </label>
                    <input type="text" name="descripcion" id='descripcion' class="form-control @error('descripcion') is-invalid @enderror" value="{{old('descripcion', $objeto->descripcion)}}" required maxlength="30">
                    @error('descripcion')
                    <span class="invalid-feedback" role="alert">
                        {{$message}}
                    </span>
                    @enderror
                </div>
            

                <div class="d-flex justify-content-center mt-4">
                    <button type="submit" class="btn btn-primary mx-2">
                    Editar
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection