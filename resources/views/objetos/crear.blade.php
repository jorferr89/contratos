@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="h1" align="center">
                Contracts
            </div>

            <div class="h3" align="center">Crear Objeto</div>

                <form class="card-body" method="post" action="{{route('objetos.guardar')}}">
            
                @csrf


                            <div class="form-group">
                                <label for="descripcion">
                                    Descripción
                                </label>
                                <input type="text" name="descripcion" id='descripcion' class="form-control @error('descripcion') is-invalid @enderror" value="{{old('descripcion')}}" required maxlength="30">
                                @error('descripcion')
                                <span class="invalid-feedback" role="alert">
                                    {{$message}}
                                </span>
                                @enderror
                            </div>
                        
                            <div class="d-flex justify-content-center mt-4">
                                <button type="submit" class="btn btn-primary mx-2">
                                Crear
                                </button>
                            </div>
                </form>
        </div>
    </div>
</div>


@endsection