@extends('layouts.app')

@section('content')

<div class="card-body">
    <div class="h1" align="center">
                Contracts
            </div>
    <div class="h3" align="center">
        Crear Persona
    </div>

    <form class="card-body" method="post" action="{{route('personas.guardar')}}">
            
            @csrf

    <div class="container-fluid mt-3">
            <div class="row py-4">
                <div class="col pr-4">

    <div class="form-group">
        <label for="apellidos">
            Apellidos (*)
        </label>
        <input type="text" name="apellidos" id='apellidos' class="form-control @error('apellidos') is-invalid @enderror" value="{{old('apellidos')}}" required maxlength="30">
        @error('apellidos')
        <span class="invalid-feedback" role="alert">
            {{$message}}
        </span>
        @enderror
    </div>

    <div class="form-group">
        <label for="nombres">
            Nombres (*)
        </label>
        <input type="text" name="nombres" id='nombres' class="form-control @error('nombres') is-invalid @enderror" value="{{old('nombres')}}" required maxlength="30">
        @error('nombres')
        <span class="invalid-feedback" role="alert">
            {{$message}}
        </span>
        @enderror
    </div>

    <div class="form-group">
        <label for="nro_documento">
            Número Documento (*)
        </label>
        <input type="text" name="nro_documento" id='nro_documento' class="form-control @error('nro_documento') is-invalid @enderror" value="{{old('nro_documento')}}" required maxlength="8" minlength="8" pattern="[0-9]+" title="Ingrese valores numericos">
        @error('nro_documento')
        <span class="invalid-feedback" role="alert">
            {{$message}}
        </span>
        @enderror
    </div>

    <div class="form-group">
        <label for="domicilio">
            Domicilio
        </label>
        <input type="text" name="domicilio" id='domicilio' class="form-control @error('domicilio') is-invalid @enderror" value="{{old('domicilio')}}" maxlength="30">
        @error('domicilio')
        <span class="invalid-feedback" role="alert">
            {{$message}}
        </span>
        @enderror
    </div>
    </div>
    <div class="col pl-4">
    <div class="form-group">

    <div class="form-group">
        <label for="nro_documento">
            Mail (*)
        </label>
        <input type="text" name="mail" id='mail' class="form-control @error('mail') is-invalid @enderror" value="{{old('mail')}}" required maxlength="30">
        @error('mail')
        <span class="invalid-feedback" role="alert">
            {{$message}}
        </span>
        @enderror
    </div>

    <div class="form-group">
        <label for="nro_documento">
            Teléfono (*)
        </label>
        <input type="text" name="telefono" id='telefono' class="form-control @error('telefono') is-invalid @enderror" value="{{old('telefono')}}" required maxlength="10" minlength="10" pattern="[0-9]+" title="Ingrese valores numericos">
        @error('telefono')
        <span class="invalid-feedback" role="alert">
            {{$message}}
        </span>
        @enderror
    </div>


	<div class="form-group">
	    <label for="fecha_desde">
	        Fecha Nacimiento
	    </label>
	    <input type="date" name="fecha_nacimiento" id='fecha_nacimiento' class="form-control @error('fecha_nacimiento') is-invalid @enderror" value="{{old('fecha_nacimiento')}}">
	    @error('fecha_nacimiento')
	    <span class="invalid-feedback" role="alert">
	        {{$message}}
	    </span>
	    @enderror
	</div>

    <div class="form-group">
        <label for="cuit">
            Cuit (sin guiones)
        </label>
        <input type="text" name="cuit" id='cuit' class="form-control @error('cuit') is-invalid @enderror" value="{{old('cuit')}}" maxlength="11" minlength="11">
        @error('cuit')
        <span class="invalid-feedback" role="alert">
            {{$message}}
        </span>
        @enderror
    </div>

    <div class="form-group">
        <label for="sexo_id">
            Sexo (*)
        </label>
        <br>
        <select id="sexo_id" name="sexo_id" class="custom-select @error('sexo_id') is-invalid @enderror" value="{{old('sexo_id')}}" required>
            <option value="" selected disabled hidden>- Seleccione un valor -</option>
                    @foreach($sexos as $s)
                        @if (old('sexo_id')== $s->id)
                            <option value="{{$s->id}}" selected>
                                {{$s->nombre}}
                            </option>
                        @else
                            <option value="{{$s->id}}">
                                {{$s->nombre}}
                            </option>
                        @endif
                    @endforeach
                </select>
        @error('sexo_id')
        <span class="invalid-feedback" role="alert">
            {{$message}}
        </span>
        @enderror
    </div>

	
    </div>
    </div>
</div>
</div>

    <div class="d-flex justify-content-center mt-4">
                <button type="submit" class="btn btn-primary mx-2">
                    Crear
                </button>
            </div>


    </form>





</div>


@endsection