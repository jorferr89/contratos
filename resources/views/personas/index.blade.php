@extends('layouts.app')
@section('content')

<div class="card-body">
    <div class="h1" align="center">
                Contracts
            </div>
    <div class="h3" align="center">
        Listado de Personas
    </div>

    <div align="lefth" class="my-3">
        <a href="{{ route('personas.crear')}}">
            <div class="btn btn-success" title="Crear Persona">
                <i class="fas fa-plus-square"></i>
            </div>
        </a>
    </div>

    @include ('layouts.mensaje')
        <table id="personas" class="table table-bordered table-hover border-dark" style="width:100%">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">
                        Apellidos
                    </th>
                    <th scope="col">
                        Nombres
                    </th>
                    <th scope="col">
                        Número Documento
                    </th>
                    <th scope="col">
                        Mail
                    </th>
                    <th scope="col">
                        Teléfono
                    </th>
                    <th scope="col">
                        Acciones
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($personas as $p)
                     <tr>
                        <td>
                            {{$p->apellidos}}
                        </td>
                        <td>
                            {{$p->nombres}}
                        </td>
                        <td>
                            {{$p->nro_documento}}
                        </td>
                        <td>
                            {{$p->mail}}
                        </td>
                        <td>
                            {{$p->telefono}}
                        </td>
                        <td style="width: 12%">
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#detalle{{$p->id}}" title="Ver Persona">
                                <i class="far fa-eye"></i>
                            </button>

                            <div class="modal fade" id="detalle{{$p->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Persona</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul>
        <li><b>Apellidos:</b> {{$p->apellidos}} </li>
        <li><b>Nombres:</b> {{$p->nombres}} </li>
        <li><b>Número Documento:</b> {{$p->nro_documento}}</li>
        <li><b>Domicilio:</b> @if (is_null($p->domicilio)) -
                        @else {{$p->domicilio}}
                    @endif
</li>
        <li><b>Mail:</b> {{$p->mail}}</li>
        <li><b>Teléfono:</b> {{$p->telefono}}</li>
        <li><b>Fecha Nacimiento:</b> @if (is_null($p->fecha_nacimiento)) -
                        @else {{DateTime::createFromFormat('Y-m-d', $p->fecha_nacimiento)->format('d/m/Y')}}
                    @endif
</li>

        <li><b>Cuit:</b> @if (is_null($p->cuit)) -
                        @else {{$p->cuit}}
                    @endif
</li>
        
        <li><b>Sexo:</b> {{$p->sexo->nombre}}</li>
        
        </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

                        <a href="{{ route('personas.editar', $p)}}">
                            <div class="btn btn-primary" title="Editar Persona">
                                <i class="fas fa-edit"></i>
                            </div>
                        </a>

                        <form class="d-inline" action="{{route('personas.eliminar', $p)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger" onclick="return confirm('¿Seguro eliminar?')" title="Eliminar Persona">
                                        <i class="fas fa-trash-alt"></i>
                                    </button>
                                </form>
                        </td>
             		</tr> 
        		@endforeach
    		</tbody>
		</table>       
</div>
	
@endsection

@section('js')

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#personas').DataTable({
                "aaSorting": [],
                language: {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });




        } );
    </script>
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@endsection

