@extends('layouts.app')
@section('content')

<div class="card-body">
    <div class="h2" align="center">
        Logs de Auditoría
    </div>

    <div class="card-body bg-info my-3">

    <form method="post" action="{{route('audits')}}"> 
        @csrf
            <div class="h4 text-center text-uppercase font-weight-bold my-2">
                Filtro
            </div>
            <div class="row">
                <div class="col-sm">

                    
                    <div class="form-group mx-sm-3 mb-2 col-lg-auto">
                      <b>Modelo</b>
                        <select id="auditable_type" name="auditable_type" class="custom-select @error('auditable_type') is-invalid @enderror">
                        <option value="" selected disabled hidden>- Seleccione un valor -</option>
                                @foreach($types as $at)
                                    @if (old('auditable_type', $auditable_type)== $at)
                                        <option value="{{$at}}" selected>
                                            App\{{$at}}
                                        </option>
                                    @else
                                        <option value="{{$at}}">
                                            App\{{$at}}
                                        </option>
                                    @endif
                                @endforeach
                        </select>
                        @error('auditable_type')
                        <span class="invalid-feedback" role="alert">
                          {{$message}}
                        </span>
                      @enderror
                    </div>

                    <div class="form-group mx-sm-3 mb-2 col-lg-auto">
                      <b>Desde</b>
                    <input type="date" name="fecha_desde" id='fecha_desde' class="form-control @error('fecha_desde') is-invalid @enderror" value="{{old('fecha_desde', $fecha_desde)}}">
                        @error('fecha_desde')
                          <span class="invalid-feedback" role="alert">
                            {{$message}}
                          </span>
                        @enderror
                    </div>
                </div>

                <div class="col-sm">
                    <div class="form-group mx-sm-3 mb-2 col-lg-auto">
                      <b>ID Tabla</b>
                        <input type="text" name="auditable_id" id='auditable_id' class="form-control @error('auditable_id') is-invalid @enderror" value="{{old('auditable_id', $auditable_id)}}">
                        @error('auditable_id')
                            <span class="invalid-feedback" role="alert">
                            {{$message}}
                            </span>
                        @enderror
                    </div>

                    <div class="form-group mx-sm-3 mb-2 col-lg-auto">
                      <b>Hasta</b>
                        <input type="date" name="fecha_hasta" id='fecha_hasta' class="form-control @error('fecha_hasta') is-invalid @enderror" value="{{old('fecha_hasta', $fecha_hasta)}}">
                                
                                @error('fecha_hasta')
                                    <span class="invalid-feedback" role="alert">
                                        {{$message}}
                                    </span>
                                @enderror
                    </div>
                </div>
            </div>
       
            <div align="center">
                <button type="submit" class="btn btn-primary mb-2 my-2">Filtrar</button>
            </div>
    </form>
</div>



        <table id="contratos" class="table table-bordered table-hover border-dark" style="width:100%">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Modelo</th>
              <th scope="col">Accion</th>
              <th scope="col">Usuario</th>
              <th scope="col">Hora</th>
              <th scope="col">Valores anteriores</th>
              <th scope="col">Valores nuevos</th>
              <th scope="col">Detalle</th>

                </tr>
            </thead>
            <tbody>
                
                @foreach($audits as $audit)
              <tr>
                <td>{{ $audit->auditable_type }} (id: {{ $audit->auditable_id }})</td>
                <td>
                  @switch ($audit->event)
                    @case ('created')
                      <span class="badge badge-success">Creación</span>
                    @break
                    @case ('updated')
                      <span class="badge badge-warning">Actualización</span>
                    @break
                    @case ('deleted')
                      <span class="badge badge-danger">Eliminación</span>
                    @break
                    @case ('restored')
                      <span class="badge badge-primary">Restauración</span>
                    @break
                  @endswitch
                </td>
                <td>
                  @if ($audit->user)
                    {{ $audit->user_id }}
                  @endif
                </td>
                <td>{{ $audit->created_at->format('d/m/Y h:i:s A') }}</td>
                <td>
                  <table class="table">
                    @foreach($audit->old_values as $attribute => $value)
                      <tr>
                        <td><b>{{ $attribute }}</b></td>
                        <td>{{ $value }}</td>
                      </tr>
                    @endforeach
                  </table>
                </td>
                <td>
                  <table class="table">
                    @foreach($audit->new_values as $attribute => $value)
                      <tr>
                        <td><b>{{ $attribute }}</b></td>
                        <td>{{ $value }}</td>
                      </tr>
                    @endforeach
                  </table>
                </td>
                <td>

                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#detalle{{$audit->id}}">
                  <i class="fas fa-info-circle"></i>
                </button>

                  <div class="modal fade" id="detalle{{$audit->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Detalle</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul>
          <li><b>URL:</b> {{$audit->url}}
          <li><b>IP:</b> {{$audit->ip_address}}
          <li><b>Navegador:</b> {{$audit->user_agent}}
        </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
                </td>
              </tr>
            @endforeach

            </tbody>
        </table>       
</div>
    
@endsection

@section('js')

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>


<script type="text/javascript">
    $(document).ready(function(){


    $('#fecha_desde').change(function() {

        var fecha_desde = $(this).val();
        $('#fecha_hasta').attr({"min" : fecha_desde});;


        });

    
    $('#fecha_hasta').change(function() {

        var fecha_hasta = $(this).val();
        $('#fecha_desde').attr({"max" : fecha_hasta});;


        });



});



</script>
    <script>
        $(document).ready(function() {
            $('#contratos').DataTable({
                "aaSorting": [],
                language: {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });




        } );
    </script>
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@endsection

