@extends('layouts.app')

@section('content')
        <div class="card-body">
            <div class="h1" align="center">
                Contracts
            </div>
    <div class="h3" align="center">
        Búsqueda Avanzada de Contratos
    </div>

<form class="card-body" method="post" action="{{route('contratos.buscar')}}">
            
            @csrf

            <div class="container-fluid mt-3">
            <div class="row py-4">
                <div class="col pr-4">

    

    

    <div class="form-group">
        <label for="fecha_desde">
            Fecha Desde 
        </label>
        <input type="date" name="fecha_desde" id='fecha_desde' class="form-control @error('fecha_desde') is-invalid @enderror" value="{{old('fecha_desde', $fecha_desde)}}">
                            
                            @error('fecha_desde')
                                <span class="invalid-feedback" role="alert">
                                    {{$message}}
                                </span>
                            @enderror


    </div>

    <div class="form-group">
        <label for="fecha_hasta">
            Fecha Hasta 
        </label>
        <input type="date" name="fecha_hasta" id='fecha_hasta' class="form-control @error('fecha_hasta') is-invalid @enderror" value="{{old('fecha_hasta', $fecha_hasta)}}">
                            
                            @error('fecha_hasta')
                                <span class="invalid-feedback" role="alert">
                                    {{$message}}
                                </span>
                            @enderror

    </div>

    <div class="form-group">
        <label for="estado_id">
            Estado 
        </label>
        <br>
        <select id="estado_id" name="estado_id" class="custom-select @error('estado_id') is-invalid @enderror">
                                <option value="" selected disabled hidden>- Seleccione un valor -</option>

                                @foreach($estados as $e)
                                    @if(!is_null($estado))

                                        @if (old('estado_id', $estado->id) == $e->id)
                                            <option value="{{$e->id}}" selected> {{$e->nombre}} </option>
                                        @else
                                            <option value="{{$e->id}}"> {{$e->nombre}} </option>
                                        @endif
                                    
                                    @else

                                        @if (old('estado_id') == $e->id)
                                            <option value="{{$e->id}}" selected> {{$e->nombre}} </option>
                                        @else
                                            <option value="{{$e->id}}"> {{$e->nombre}} </option>
                                        @endif
                                    @endif
                                @endforeach
                            </select>

                            @error('estado_id')
                                <span class="invalid-feedback" role="alert">
                                    {{$message}}
                                </span>
                            @enderror

    </div>

    
    </div>
    <div class="col pl-4">
    

    

    

    <div class="form-group">
        <label for="solicitante_id">
            Solicitante
        </label>
        <br>
        <select id="solicitante_id" name="solicitante_id" class="custom-select @error('solicitante_id') is-invalid @enderror">
                                <option value="" selected disabled hidden>- Seleccione un valor -</option>

                                @foreach($solicitantes as $s)
                                    @if(!is_null($solicitante))

                                        @if (old('solicitantes_id', $solicitante->id) == $s->id)
                                            <option value="{{$s->id}}" selected> {{$s->nombre}} </option>
                                        @else
                                            <option value="{{$s->id}}"> {{$s->nombre}} </option>
                                        @endif
                                    
                                    @else

                                        @if (old('solicitantes_id') == $s->id)
                                            <option value="{{$s->id}}" selected> {{$s->nombre}} </option>
                                        @else
                                            <option value="{{$s->id}}"> {{$s->nombre}} </option>
                                        @endif
                                    @endif
                                @endforeach
                            </select>

                            @error('solicitantes_id')
                                <span class="invalid-feedback" role="alert">
                                    {{$message}}
                                </span>
                            @enderror

        
    </div>

    <div class="form-group">
        <label for="objeto_id">
            Objeto
        </label>
        <br>
        <select id="objeto_id" name="objeto_id" class="custom-select @error('objeto_id') is-invalid @enderror">
                                <option value="" selected disabled hidden>- Seleccione un valor -</option>

                                @foreach($objetos as $o)
                                    @if(!is_null($objeto))

                                        @if (old('objeto_id', $objeto->id) == $o->id)
                                            <option value="{{$o->id}}" selected> {{$o->descripcion}} </option>
                                        @else
                                            <option value="{{$o->id}}"> {{$o->descripcion}} </option>
                                        @endif
                                    
                                    @else

                                        @if (old('objeto_id') == $o->id)
                                            <option value="{{$o->id}}" selected> {{$o->nombre}} </option>
                                        @else
                                            <option value="{{$o->id}}"> {{$o->descripcion}} </option>
                                        @endif
                                    @endif
                                @endforeach
                            </select>

                            @error('objeto_id')
                                <span class="invalid-feedback" role="alert">
                                    {{$message}}
                                </span>
                            @enderror
    </div>

    <div class="form-group">
        <label for="persona_id">
            Persona 
        </label>
        <br>
        <select id="persona_id" name="persona_id" class="custom-select @error('persona_id') is-invalid @enderror">
                                <option value="" selected disabled hidden>- Seleccione un valor -</option>

                                @foreach($personas as $p)
                                    @if(!is_null($persona))

                                        @if (old('persona_id', $persona->id) == $p->id)
                                            <option value="{{$p->id}}" selected> {{$p->apellidos}} {{$p->nombres}} </option>
                                        @else
                                            <option value="{{$p->id}}"> {{$p->apellidos}} {{$p->nombres}} </option>
                                        @endif
                                    
                                    @else

                                        @if (old('persona_id') == $p->id)
                                            <option value="{{$o->id}}" selected> {{$p->apellidos}} {{$p->nombres}} </option>
                                        @else
                                            <option value="{{$p->id}}"> {{$p->apellidos}} {{$p->nombres}} </option>
                                        @endif
                                    @endif
                                @endforeach
                            </select>

                            @error('persona_id')
                                <span class="invalid-feedback" role="alert">
                                    {{$message}}
                                </span>
                            @enderror
    </div>
    </div>
    </div>

    <div class="d-flex justify-content-center mt-4">
                <button type="submit" class="btn btn-primary mx-2">
                    Filtrar
                </button>
            </div>
    </form>
       

            
</div>

@if (!is_null($resultado))
                    <div class="card mt-3">
                        

                        

                        <div class="card-body">

            
                        


                
                    <table id="contratos" class="table table-bordered table-hover border-dark" style="width:100%">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">
                        #
                    </th>
                    <th scope="col">
                        Fecha Desde
                    </th>
                    <th scope="col">
                        Fecha Hasta
                    </th>
                    <th scope="col">
                        Nro. Disp.
                    </th>
                    <th scope="col">
                        Estado
                    </th>
                    <th scope="col">
                        Solicitante
                    </th>
                    <th scope="col">
                        Objeto
                    </th>
                    <th scope="col">
                        Persona
                    </th>
                    <th scope="col">
                        Ver
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($resultado as $c)
                     <tr>
                        <td>
                            {{$c->nro_contrato}}
                        </td>
                        <td>
                            <span class="d-none">
                                    {{$c->fecha_desde}}
                                </span>

                            {{DateTime::createFromFormat('Y-m-d', $c->fecha_desde)->format('d/m/Y')}}
                        </td>
                        <td>
                            <span class="d-none">
                                    {{$c->fecha_hasta}}
                                </span>

                            {{DateTime::createFromFormat('Y-m-d', $c->fecha_hasta)->format('d/m/Y')}}
                        </td>
                        <td>
                            @if (is_null($c->nro_disp)) -
                        @else {{$c->nro_disp}}
                    @endif
                        </td>
                        <td>
                            {{$c->estado->nombre}}
                        </td>
                        <td>
                        @if (is_null($c->solicitante)) -
                        @else {{$c->solicitante->nombre}}
                    @endif
                </td>
                        <td>@if (is_null($c->objeto)) -
                        @else {{$c->objeto->descripcion}}
                    @endif
                </td>
                        <td>
                            {{$c->persona->apellidos}} {{$c->persona->nombres}}
                        </td>
                        <td>
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#detalle{{$c->id}}" title="Ver Contrato">
                                <i class="far fa-eye"></i>
                            </button>

                            <div class="modal fade" id="detalle{{$c->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Contrato #{{$c->nro_contrato}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul>
        <li><b>Contrato:</b> {{$c->contrato}} </li>
        <li><b>Fecha Desde:</b> {{DateTime::createFromFormat('Y-m-d', $c->fecha_desde)->format('d/m/Y')}} </li>
        <li><b>Fecha Hasta:</b> {{DateTime::createFromFormat('Y-m-d', $c->fecha_hasta)->format('d/m/Y')}}</li>
        <li><b>Fecha Carga:</b> {{DateTime::createFromFormat('Y-m-d', $c->fecha_carga)->format('d/m/Y')}}</li>
        <li><b>Fecha Disposición:</b> @if (is_null($c->fecha_disp)) -
                        @else {{DateTime::createFromFormat('Y-m-d', $c->fecha_disp)->format('d/m/Y')}}
                    @endif
</li>
        <li><b>Número Disposición:</b> @if (is_null($c->nro_disp)) -
                        @else {{$c->nro_disp}}
                    @endif</li>
        <li><b>Resumen:</b> @if (is_null($c->resumen)) -
                        @else {{$c->resumen}}
                    @endif</li>
        <li><b>Nombre de Archivo:</b> @if (is_null($c->nombre_archivo)) -
                        @else {{$c->nombre_archivo}}
                    @endif</li>
        <li><b>Anexo:</b> @if ($c->tiene_anexo==0) <span class="badge badge-danger">No</span>
                            @else <span class="badge badge-success">Si</span>
                            @endif</li>
        <li><b>Estado:</b> {{$c->estado->nombre}}</li>
        <li><b>Solicitante:</b> @if (is_null($c->solicitante)) -
                        @else {{$c->solicitante->nombre}}
                    @endif</li>
        <li><b>Objeto:</b> @if (is_null($c->objeto)) -
                        @else {{$c->objeto->descripcion}}
                    @endif</li>
        <li><b>Persona:</b> {{$c->persona->apellidos}} {{$c->persona->nombres}}</li>
        </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

                        
    
    
    </div>
  </div>
</div>
                        


                        


                        </td>
                    </tr> 
                @endforeach
            </tbody>
        </table>       
    </div>
@endif


@endsection

@section('js')

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>


<script type="text/javascript">
    $(document).ready(function(){


    $('#fecha_desde').change(function() {

        var fecha_desde = $(this).val();
        $('#fecha_hasta').attr({"min" : fecha_desde});;


        });

    
    $('#fecha_hasta').change(function() {

        var fecha_hasta = $(this).val();
        $('#fecha_desde').attr({"max" : fecha_hasta});;


        });



});



</script>


    <script>
        $(document).ready(function() {
            $('#contratos').DataTable({
                "aaSorting": [],
                language: {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });




        } );
    </script>
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@endsection


