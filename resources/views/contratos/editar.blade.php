@extends('layouts.app')

@section('content')

<div class="card-body">
    <div class="h1" align="center">
                Contracts
            </div>
    <div class="h3" align="center">
        Editar Contrato
    </div>

<form class="card-body" method="post" action="{{route('contratos.actualizar', $contrato)}}">
            
            {{ csrf_field() }}
            {{ method_field('put') }}

            <div class="container-fluid mt-3">
            <div class="row py-4">
                <div class="col pr-4">

    <div class="form-group">
        <label for="nro_contrato">
            Número (*)
        </label>
        <input type="text" name="nro_contrato" id='nro_contrato' class="form-control @error('nro_contrato') is-invalid @enderror" value="{{old('nro_contrato', $contrato->nro_contrato)}}" readonly>
        @error('nro_contrato')
        <span class="invalid-feedback" role="alert">
            {{$message}}
        </span>
        @enderror
    </div>

    <div class="form-group">
        <label for="contrato">
            Contrato (*)
        </label>
        <input type="text" name="contrato" id='contrato' class="form-control @error('contrato') is-invalid @enderror" value="{{old('contrato', $contrato->contrato)}}" required maxlength="30">
        @error('contrato')
        <span class="invalid-feedback" role="alert">
            {{$message}}
        </span>
        @enderror
    </div>

    <div class="form-group">
        <label for="fecha_desde">
            Fecha Desde (*)
        </label>
        <input type="date" name="fecha_desde" id='fecha_desde' class="form-control @error('fecha_desde') is-invalid @enderror" value="{{old('fecha_desde', $contrato->fecha_desde)}}" readonly>
        @error('fecha_limite')
        <span class="invalid-feedback" role="alert">
            {{$message}}
        </span>
        @enderror
    </div>

    <div class="form-group">
        <label for="fecha_hasta">
            Fecha Hasta (*)
        </label>
        <input type="date" name="fecha_hasta" id='fecha_hasta' class="form-control @error('fecha_hasta') is-invalid @enderror" value="{{old('fecha_hasta', $contrato->fecha_hasta)}}" readonly>
        @error('fecha_hasta')
        <span class="invalid-feedback" role="alert">
            {{$message}}
        </span>
        @enderror
    </div>

    <div class="form-group">
        <label for="fecha_disp">
            Fecha Disposición
        </label>
        <input type="date" name="fecha_disp" id='fecha_disp' class="form-control @error('fecha_disp') is-invalid @enderror" value="{{old('fecha_disp', $contrato->fecha_disp)}}" >
        @error('fecha_disp')
        <span class="invalid-feedback" role="alert">
            {{$message}}
        </span>
        @enderror
    </div>

    <div class="form-group">
        <label for="nro_disp">
            Número Disposición
        </label>
        <input type="text" name="nro_disp" id='nro_disp' class="form-control @error('nro_disp') is-invalid @enderror" value="{{old('nro_disp', $contrato->nro_disp)}}" maxlength="10">
        @error('nro_disp')
        <span class="invalid-feedback" role="alert">
            {{$message}}
        </span>
        @enderror
    </div>
    </div>
    <div class="col pl-4">
    <div class="form-group">
        <label for="resumen">
            Resumen
        </label>
        <input type="text" name="resumen" id='resumen' class="form-control @error('resumen') is-invalid @enderror" value="{{old('resumen', $contrato->resumen)}}" maxlength="30">
        @error('resumen')
        <span class="invalid-feedback" role="alert">
            {{$message}}
        </span>
        @enderror
    </div>

    <div class="form-group">
        <label for="nombre_archivo">
            Nombre de Archivo
        </label>
        <input type="text" name="nombre_archivo" id='nombre_archivo' class="form-control @error('nombre_archivo') is-invalid @enderror" value="{{old('nombre_archivo', $contrato->nombre_archivo)}}" maxlength="30">
        @error('nombre_archivo')
        <span class="invalid-feedback" role="alert">
            {{$message}}
        </span>
        @enderror
    </div>

    <div class="form-group">
        <label for="estado_id">
            Estado (*)
        </label>
        <br>
        <select id="estado_id" name="estado_id" class="custom-select @error('estado_id') is-invalid @enderror" value="{{old('estado_id', $contrato->estado_id)}}" required>
            <option value="" selected disabled hidden>- Seleccione un valor -</option>
                    @foreach($estados as $e)
                        @if (old('estados', $contrato->estado_id)== $e->id)
                            <option value="{{$e->id}}" selected>
                                {{$e->nombre}}
                            </option>
                        @else
                            <option value="{{$e->id}}">
                                {{$e->nombre}}
                            </option>
                        @endif
                    @endforeach
                </select>
        @error('estado_id')
        <span class="invalid-feedback" role="alert">
            {{$message}}
        </span>
        @enderror
    </div>

    <div class="form-group">
        <label for="solicitante_id">
            Solicitante
        </label>
        <br>
        <select id="solicitante_id" name="solicitante_id" class="custom-select @error('solitante_id') is-invalid @enderror" value="{{old('solicitante_id', $contrato->solicitante_id)}}">
            <option value="" selected disabled hidden>- Seleccione un valor -</option>
                    @foreach($solicitantes as $s)
                        @if (old('solicitantes', $contrato->solicitante_id)== $s->id)
                            <option value="{{$s->id}}" selected>
                                {{$s->nombre}}
                            </option>
                        @else
                            <option value="{{$s->id}}">
                                {{$s->nombre}}
                            </option>
                        @endif
                    @endforeach
                </select>
        @error('solicitante_id')
        <span class="invalid-feedback" role="alert">
            {{$message}}
        </span>
        @enderror
    </div>

    <div class="form-group">
        <label for="objeto_id">
            Objeto
        </label>
        <br>
        <select id="objeto_id" name="objeto_id" class="custom-select @error('objeto_id') is-invalid @enderror" value="{{old('objeto_id', $contrato->objeto_id)}}">
            <option value="" selected disabled hidden>- Seleccione un valor -</option>
                    @foreach($objetos as $o)
                        @if (old('objetos', $contrato->objeto_id)== $o->id)
                            <option value="{{$o->id}}" selected>
                                {{$o->descripcion}}
                            </option>
                        @else
                            <option value="{{$o->id}}">
                                {{$o->descripcion}}
                            </option>
                        @endif
                    @endforeach
                </select>
        @error('objeto_id')
        <span class="invalid-feedback" role="alert">
            {{$message}}
        </span>
        @enderror
    </div>

    <div class="form-group">
        <label for="persona_id">
            Persona (*)
        </label>
        <br>
        <select id="persona_id" name="persona_id" class="custom-select @error('persona_id') is-invalid @enderror" value="{{old('persona_id', $contrato->persona_id)}}" required>
            <option value="" selected disabled hidden>- Seleccione un valor -</option>
                    @foreach($personas as $p)
                        @if (old('personas', $contrato->persona_id)== $p->id)
                            <option value="{{$p->id}}" selected>
                                {{$p->apellidos}} {{$p->nombres}}
                            </option>
                        @else
                            <option value="{{$p->id}}">
                                {{$p->apellidos}} {{$p->nombres}}
                            </option>
                        @endif
                    @endforeach
                </select>
        @error('persona_id')
        <span class="invalid-feedback" role="alert">
            {{$message}}
        </span>
        @enderror
    </div>
    </div>
    </div>

    <div class="d-flex justify-content-center mt-4">
                <button type="submit" class="btn btn-primary mx-2">
                    Editar
                </button>
            </div>
    </form>

@endsection
