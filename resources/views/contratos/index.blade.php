@extends('layouts.app')
@section('content')

<div class="card-body">
    <div class="h1" align="center">
                Contracts
            </div>
    <div class="h3" align="center">
        Listado de Contratos
    </div>

    <div align="lefth" class="my-3">
        <a href="{{ route('contratos.crear')}}">
            <div class="btn btn-success" title="Crear Contrato">
                <i class="fas fa-plus-square"></i>
            </div>
        </a>

        <a href="{{ route('contratos.buscar')}}">
            <div class="btn btn-secondary" title="Búsqueda Avanzada">
                <i class="fas fa-search"></i>
            </div>
        </a>
    </div>
    
    @include ('layouts.mensaje')

    <form class="form-inline mx-auto bg-light mb-5 py-3" method="post" action="{{route('contratos')}}"> 
          @csrf
          <div class="col-12 col-lg-auto  text-center text-uppercase font-weight-bold">
                Filtrar registros por fecha
           </div>
          <div class="col-2 text-right my-auto">
            Desde
          </div>
          <div class="form-group col-2">
            <input type="date" name="fecha_desde" id='fecha_desde' class="form-control @error('fecha_desde') is-invalid @enderror" value="{{old('fecha_desde', $fecha_desde)}}">
                            
                            
          </div>

          <div class="col-2 text-right my-auto">
            Hasta
          </div>
          <div class="form-group col-2">
            <input type="date" name="fecha_hasta" id='fecha_hasta' class="form-control @error('fecha_hasta') is-invalid @enderror" value="{{old('fecha_hasta', $fecha_hasta)}}">
                            
                            
          </div>
          <div class="col-1">
            <button type="submit" class="btn btn-primary mb-2">Filtrar</button>
          </div>
        </form>


        <table id="contratos" class="table table-bordered table-hover border-dark" style="width:100%">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">
                        #
                    </th>
                    <th scope="col">
                        Fecha Desde
                    </th>
                    <th scope="col">
                        Fecha Hasta
                    </th>
                    <th scope="col">
                        Nro. Disp.
                    </th>
                    <th scope="col">
                        Estado
                    </th>
                    <th scope="col">
                        Solicitante
                    </th>
                    <th scope="col">
                        Objeto
                    </th>
                    <th scope="col">
                        Persona
                    </th>
                    <th scope="col">
                        Anexo
                    </th>
                    <th scope="col">
                        Acciones
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($contratos as $c)
                     <tr>
                        <td>
                            {{$c->nro_contrato}}
                        </td>
                        <td>
                            <span class="d-none">
                                    {{$c->fecha_desde}}
                                </span>

                            {{DateTime::createFromFormat('Y-m-d', $c->fecha_desde)->format('d/m/Y')}}
                        </td>
                        <td>
                            <span class="d-none">
                                    {{$c->fecha_hasta}}
                                </span>

                            {{DateTime::createFromFormat('Y-m-d', $c->fecha_hasta)->format('d/m/Y')}}
                        </td>
                        <td>
                            @if (is_null($c->nro_disp)) -
                        @else {{$c->nro_disp}}
                    @endif
                        </td>
                        <td>
                            {{$c->estado->nombre}}
                        </td>
                        <td>
                        @if (is_null($c->solicitante)) -
                        @else {{$c->solicitante->nombre}}
                    @endif
                </td>
                        <td>@if (is_null($c->objeto)) -
                        @else {{$c->objeto->descripcion}}
                    @endif
                </td>
                        <td>
                            {{$c->persona->apellidos}} {{$c->persona->nombres}}
                        </td>

                        <td style="width: 8%">
                            @if ($c->tiene_anexo==0) <span class="badge badge-danger">No</span>
                            @else <span class="badge badge-success">Si</span>
                            @endif</li>

                            @if ($c->tiene_anexo==0) 
                            

                        <button type="button" class="btn btn-ligth" data-toggle="modal" data-target="#agregarAnexo{{$c->id}}" title="Agregar Anexo">
                                <i class="fas fa-paperclip"></i>
                            </button>

                    <div class="modal fade" id="agregarAnexo{{$c->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <form method="post" action="{{route('contratos.guardarAnexo', $c)}}" enctype="multipart/form-data">
                                @csrf
                                <div class="modal-body">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel"> Agregar Anexo al Contrato #{{$c->nro_contrato}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body" style="color: black">
                                
                                            <input type="file" class="form-control-file" name="anexo" required>
                                            <p class="text-danger font-italic">(*) Se aceptan solamente archivos .PDF</p>
                                        </div>
                                       
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">    Cerrar
                                            </button>
                                            <button type="submit" class="btn btn-primary mx-2">
                                                Guardar
                                            </button>
                                        </div>
                                    
    
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                        @else 
                            @endif
                        </td>

                        <td style="width: 12%">
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#detalle{{$c->id}}" title="Ver Contrato">
                                <i class="far fa-eye"></i>
                            </button>

                            <div class="modal fade" id="detalle{{$c->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Contrato #{{$c->nro_contrato}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul>
        <li><b>Contrato:</b> {{$c->contrato}} </li>
        <li><b>Fecha Desde:</b> {{DateTime::createFromFormat('Y-m-d', $c->fecha_desde)->format('d/m/Y')}} </li>
        <li><b>Fecha Hasta:</b> {{DateTime::createFromFormat('Y-m-d', $c->fecha_hasta)->format('d/m/Y')}}</li>
        <li><b>Fecha Carga:</b> {{DateTime::createFromFormat('Y-m-d', $c->fecha_carga)->format('d/m/Y')}}</li>
        <li><b>Fecha Disposición:</b> @if (is_null($c->fecha_disp)) -
                        @else {{DateTime::createFromFormat('Y-m-d', $c->fecha_disp)->format('d/m/Y')}}
                    @endif
</li>
        <li><b>Número Disposición:</b> @if (is_null($c->nro_disp)) -
                        @else {{$c->nro_disp}}
                    @endif</li>
        <li><b>Resumen:</b> @if (is_null($c->resumen)) -
                        @else {{$c->resumen}}
                    @endif</li>
        <li><b>Nombre de Archivo:</b> @if (is_null($c->nombre_archivo)) -
                        @else {{$c->nombre_archivo}}
                    @endif</li>
        <li><b>Anexo:</b> @if ($c->tiene_anexo==0) <span class="badge badge-danger">No</span>
                            @else <span class="badge badge-success">Si</span>
                            @endif</li>
        <li><b>Estado:</b> {{$c->estado->nombre}}</li>
        <li><b>Solicitante:</b> @if (is_null($c->solicitante)) -
                        @else {{$c->solicitante->nombre}}
                    @endif</li>
        <li><b>Objeto:</b> @if (is_null($c->objeto)) -
                        @else {{$c->objeto->descripcion}}
                    @endif</li>
        <li><b>Persona:</b> {{$c->persona->apellidos}} {{$c->persona->nombres}}</li>
        </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

                        <a href="{{ route('contratos.editar', $c)}}">
                            <div class="btn btn-primary" title="Editar Contrato">
                                <i class="fas fa-edit"></i>
                            </div>
                        </a>

                        


                        <form class="d-inline" action="{{route('contratos.eliminar', $c)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger" onclick="return confirm('¿Seguro eliminar?')" title="Eliminar Contrato">
                                        <i class="fas fa-trash-alt"></i>
                                    </button>
                                </form>


                        </td>
             		</tr> 
        		@endforeach
    		</tbody>
		</table>       
</div>
	
@endsection

@section('js')

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>


<script type="text/javascript">
    $(document).ready(function(){


    $('#fecha_desde').change(function() {

        var fecha_desde = $(this).val();
        $('#fecha_hasta').attr({"min" : fecha_desde});;


        });

    
    $('#fecha_hasta').change(function() {

        var fecha_hasta = $(this).val();
        $('#fecha_desde').attr({"max" : fecha_hasta});;


        });



});



</script>

    <script>
        $(document).ready(function() {
            $('#contratos').DataTable({
                "aaSorting": [],
                language: {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });




        } );
    </script>
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@endsection

