@extends('layouts.app')

@section('content')

<div class="card-body">
    <div class="h1" align="center">
                Contracts
            </div>
    <div class="h3" align="center">
        Crear Contrato
    </div>

    <form class="card-body" method="post" action="{{route('contratos.guardar')}}">
            
            @csrf

    <div class="container-fluid mt-3">
            <div class="row py-4">
                <div class="col pr-4">

    

    <div class="form-group">
        <label for="contrato">
            Contrato (*)
        </label>
        <input type="text" name="contrato" id='contrato' class="form-control @error('contrato') is-invalid @enderror" value="{{old('contrato')}}" required maxlength="30">
        @error('contrato')
        <span class="invalid-feedback" role="alert">
            {{$message}}
        </span>
        @enderror
    </div>

	<div class="form-group">
	    <label for="fecha_desde">
	        Fecha Desde (*)
	    </label>
	    <input type="date" name="fecha_desde" id='fecha_desde' class="form-control @error('fecha_desde') is-invalid @enderror" value="{{old('fecha_desde')}}" required>
	    @error('fecha_limite')
	    <span class="invalid-feedback" role="alert">
	        {{$message}}
	    </span>
	    @enderror
	</div>

	<div class="form-group">
	    <label for="fecha_hasta">
	        Fecha Hasta (*)
	    </label>
	    <input type="date" name="fecha_hasta" id='fecha_hasta' class="form-control @error('fecha_hasta') is-invalid @enderror" value="{{old('fecha_hasta')}}" required>
	    @error('fecha_hasta')
	    <span class="invalid-feedback" role="alert">
	        {{$message}}
	    </span>
	    @enderror
	</div>

	<div class="form-group">
	    <label for="fecha_disp">
	        Fecha Disposición
	    </label>
	    <input type="date" name="fecha_disp" id='fecha_disp' class="form-control @error('fecha_disp') is-invalid @enderror" value="{{old('fecha_disp')}}">
	    @error('fecha_disp')
	    <span class="invalid-feedback" role="alert">
	        {{$message}}
	    </span>
	    @enderror
	</div>

	<div class="form-group">
        <label for="nro_disp">
            Número Disposición
        </label>
        <input type="text" name="nro_disp" id='nro_disp' class="form-control @error('nro_disp') is-invalid @enderror" value="{{old('nro_disp')}}" maxlength="10" pattern="[0-9]+" title="Ingrese valores numericos">
        @error('nro_disp')
        <span class="invalid-feedback" role="alert">
            {{$message}}
        </span>
        @enderror
    </div>
    </div>
    <div class="col pl-4">
    <div class="form-group">
        <label for="resumen">
            Resumen
        </label>
        <input type="text" name="resumen" id='resumen' class="form-control @error('resumen') is-invalid @enderror" value="{{old('resumen')}}" maxlength="30">
        @error('resumen')
        <span class="invalid-feedback" role="alert">
            {{$message}}
        </span>
        @enderror
    </div>

    <div class="form-group">
        <label for="nombre_archivo">
            Nombre de Archivo
        </label>
        <input type="text" name="nombre_archivo" id='nombre_archivo' class="form-control @error('nombre_archivo') is-invalid @enderror" value="{{old('nombre_archivo')}}" maxlength="30">
        @error('nombre_archivo')
        <span class="invalid-feedback" role="alert">
            {{$message}}
        </span>
        @enderror
    </div>

    <div class="form-group">
        <label for="estado_id">
            Estado (*)
        </label>
        <br>
        <select id="estado_id" name="estado_id" class="custom-select @error('estado_id') is-invalid @enderror" value="{{old('estado_id')}}" required>
            <option value="" selected disabled hidden>- Seleccione un valor -</option>
                    @foreach($estados as $e)
                        @if (old('estado_id')== $e->id)
                            <option value="{{$e->id}}" selected>
                                {{$e->nombre}}
                            </option>
                        @else
                            <option value="{{$e->id}}">
                                {{$e->nombre}}
                            </option>
                        @endif
                    @endforeach
                </select>
        @error('estado_id')
        <span class="invalid-feedback" role="alert">
            {{$message}}
        </span>
        @enderror
    </div>

    <div class="form-group">
        <label for="solicitante_id">
            Solicitante
        </label>
        <br>
        <select id="solicitante_id" name="solicitante_id" class="custom-select @error('solicitante_id') is-invalid @enderror" value="{{old('solicitante_id')}}">
            <option value="" selected disabled hidden>- Seleccione un valor -</option>
                    @foreach($solicitantes as $s)
                        @if (old('solicitante_id')== $s->id)
                            <option value="{{$s->id}}" selected>
                                {{$s->nombre}}
                            </option>
                        @else
                            <option value="{{$s->id}}">
                                {{$s->nombre}}
                            </option>
                        @endif
                    @endforeach
                </select>
        @error('solicitante_id')
        <span class="invalid-feedback" role="alert">
            {{$message}}
        </span>
        @enderror
    </div>

    <div class="form-group">
        <label for="objeto_id">
            Objeto
        </label>
        <br>
        <select id="objeto_id" name="objeto_id" class="custom-select @error('objeto_id') is-invalid @enderror" value="{{old('objeto_id')}}">
            <option value="" selected disabled hidden>- Seleccione un valor -</option>
                    @foreach($objetos as $o)
                        @if (old('objeto_id')== $o->id)
                            <option value="{{$o->id}}" selected>
                                {{$o->descripcion}}
                            </option>
                        @else
                            <option value="{{$o->id}}">
                                {{$o->descripcion}}
                            </option>
                        @endif
                    @endforeach
                </select>
        @error('objeto_id')
        <span class="invalid-feedback" role="alert">
            {{$message}}
        </span>
        @enderror
    </div>

    <div class="form-group">
        <label for="persona_id">
            Persona (*)
        </label>
        <br>
        <select id="persona_id" name="persona_id" class="custom-select @error('persona_id') is-invalid @enderror" value="{{old('persona_id')}}" required>
            <option value="" selected disabled hidden>- Seleccione un valor -</option>
                    @foreach($personas as $p)
                        @if (old('persona_id')== $p->id)
                            <option value="{{$p->id}}" selected>
                                {{$p->apellidos}} {{$p->nombres}}
                            </option>
                        @else
                            <option value="{{$p->id}}">
                                {{$p->apellidos}} {{$p->nombres}}
                            </option>
                        @endif
                    @endforeach
                </select>
        @error('persona_id')
        <span class="invalid-feedback" role="alert">
            {{$message}}
        </span>
        @enderror
    </div>
    </div>
    </div>

    <div class="d-flex justify-content-center mt-4">
                <button type="submit" class="btn btn-primary mx-2">
                    Crear
                </button>
            </div>


    </form>





</div>


@endsection

@section('js')

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){


    $('#fecha_desde').change(function() {

        var fecha_desde = $(this).val();
        $('#fecha_hasta').attr({"min" : fecha_desde});;


        });

    
    $('#fecha_hasta').change(function() {

        var fecha_hasta = $(this).val();
        $('#fecha_desde').attr({"max" : fecha_hasta});;


        });



});



</script>

@endsection