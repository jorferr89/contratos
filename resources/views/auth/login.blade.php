@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="h1" align="center">
                Contracts
            </div>

            <div class="h4" align="center">Inicio de Sesión</div>


                <form method="POST" action="{{ route('login') }}">
            
                    @csrf

                    <div class="row justify-content-center">
                        <div class="col-md-6">
                            <div class="col pr-4">

                                <div class="form-group">
                                    <label for="nombre">
                                        Correo Electrónico
                                    </label>
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" required autocomplete="email" maxlength="30">

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="nombre">
                                        Contraseña
                                    </label>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" maxlength="30">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-center mt-4">
                    <button type="submit" class="btn btn-primary">
                                    {{ __('Ingresar') }}
                    </button>

                    
                    </div>
                </form>
                <div class="d-flex justify-content-center mt-4">

                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal" title="Ayuda">
  <i class="fas fa-question-circle"></i>
</button>
                </div>

                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Lista de Usuarios</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul>
                
                    <li>Administrador: admin@mail.com</li>
            
                    <li>Contrato: contrato@mail.com</li>
            
                    <li>Auditor: audit@mail.com</li>

                    
                </ul>
                <b>Contraseña:</b> 12345678

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        
      </div>
    </div>
  </div>
</div>
            </div>
        </div>
    </div>
</div>


@endsection
