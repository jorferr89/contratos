<div class="container">
    <a class="navbar-brand" href="{{ url('/home') }}">
        Contracts
    </a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <!-- Left Side Of Navbar -->
        <ul class="navbar-nav mr-auto">

        </ul>

        <!-- Right Side Of Navbar -->
        <ul class="navbar-nav ml-auto">
            <!-- Authentication Links -->
            @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">Iniciar sesión</a>
                </li>
                    
            @else

                @if (auth()->user()->rol_id==3)

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('audits') }}"> <i class="fas fa-user-secret"></i>&nbsp; Logs </a>
                </li>
                @endif

                @if (auth()->user()->rol_id==2)

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('contratos') }}"> <i class="fas fa-file-contract"></i> Contratos</a>
                </li>

                @endif

                @if (auth()->user()->rol_id==1)
                
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('estados') }}"> <i class="fas fa-stream"></i> Estados</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('objetos') }}"> <i class="fas fa-cube"></i> Objetos</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('personas') }}"> <i class="fas fa-users"></i> Personas</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('sexos') }}"> <i class="fas fa-people-arrows"></i> Sexos</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('solicitantes') }}"> <i class="fas fa-user-tie"></i> Solicitantes</a>
                </li>

                @endif

                
                        
                <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <i class="fas fa-user"></i> {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                
                                    

                                

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                                    <a class="dropdown-item" href="{{ route('home') }}">
                            <i class="fas fa-home"></i>&nbsp; Inicio
                        </a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                            <i class="fas fa-sign-out-alt"></i>&nbsp; Salir
                        </a>


                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
            @endguest
        </ul>
    </div>
</div>
