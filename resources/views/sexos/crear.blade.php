@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="h1" align="center">
                Contracts
            </div>

            <div class="h3" align="center">Crear Sexo</div>

                <form class="card-body" method="post" action="{{route('sexos.guardar')}}">
            
                @csrf

                            <div class="form-group">
                                <label for="nombre">
                                    Nombre
                                </label>
                                <input type="text" name="nombre" id='nombre' class="form-control @error('nombre') is-invalid @enderror" value="{{old('nombre')}}" required maxlength="30">
                                @error('nombre')
                                <span class="invalid-feedback" role="alert">
                                    {{$message}}
                                </span>
                                @enderror
                            </div>
                        
                    
            
                    <div class="d-flex justify-content-center mt-4">
                        <button type="submit" class="btn btn-primary mx-2">
                            Crear
                        </button>
                    </div>
                </form>
        </div>
    </div>
</div>

@endsection