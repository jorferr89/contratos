@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="h1" align="center">
                Contracts
            </div>

            <div class="h3" align="center">Editar Sexo</div>

            <form class="card-body" method="post" action="{{route('sexos.actualizar', $sexo)}}">
            
            {{ csrf_field() }}
            {{ method_field('put') }}

                

                <div class="form-group">
                    <label for="nombre">
                        Nombre
                    </label>
                    <input type="text" name="nombre" id='nombre' class="form-control @error('nombre') is-invalid @enderror" value="{{old('nombre', $sexo->nombre)}}" required maxlength="30">
                    @error('nombre')
                    <span class="invalid-feedback" role="alert">
                        {{$message}}
                    </span>
                    @enderror
                </div>


                <div class="d-flex justify-content-center mt-4">
                <button type="submit" class="btn btn-primary mx-2">
                    Editar
                </button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection