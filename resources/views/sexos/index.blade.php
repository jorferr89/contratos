@extends('layouts.app')
@section('content')

<div class="card-body">
    <div class="h1" align="center">
                Contracts
            </div>
    <div class="h3" align="center">
        Listado de Sexos
    </div>

    <div align="lefth" class="my-3">
        <a href="{{ route('sexos.crear')}}">
            <div class="btn btn-success" title="Crear Sexo">
                <i class="fas fa-plus-square"></i>
            </div>
        </a>
    </div>

    @include('layouts.mensaje')
        <table id="sexos" class="table table-bordered table-hover border-dark" style="width:100%">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">
                        Nombre
                    </th>
                    <th scope="col">
                        Acciones
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($sexos as $s)
                     <tr>
                        <td>
                            {{$s->nombre}}
                        </td>
                        <td style="width: 10%" align="center">
                            <a href="{{ route('sexos.editar', $s)}}">
                            <div class="btn btn-primary" title="Editar Sexo">
                                <i class="fas fa-edit"></i>
                            </div>
                        </a>

                        <form class="d-inline" action="{{route('sexos.eliminar', $s)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger" onclick="return confirm('¿Seguro eliminar?')" title="Eliminar Sexo">
                                        <i class="fas fa-trash-alt"></i>
                                    </button>
                                </form>
                        </td>
             		</tr> 
        		@endforeach
    		</tbody>
		</table>       
</div>
	
@endsection

@section('js')

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#sexos').DataTable({
                "aaSorting": [],
                language: {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });




        } );
    </script>
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@endsection

