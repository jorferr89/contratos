<?php

use Illuminate\Database\Seeder;
use App\Solicitante;

class SolicitanteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('solicitantes')->insert([
        'nombre'=> 'Sec. Extensión',
        ]);

        DB::table('solicitantes')->insert([
        'nombre'=> 'Sec. Académica',
        ]);

        DB::table('solicitantes')->insert([
        'nombre'=> 'Sec. Administrativa',
        ]);

        DB::table('solicitantes')->insert([
        'nombre'=> 'Sec. Posgrado',
        ]);

    }
}
