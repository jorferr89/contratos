<?php

use Illuminate\Database\Seeder;
use App\Contrato;

class ContratoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('contratos')->insert([
        'nro_contrato'=> '1/2020',
        'contrato'=> 'Contrato1',
        'fecha_desde'=> '2020-01-01',
        'fecha_hasta'=> '2020-02-03',
        'fecha_carga'=> '2020-01-01',
        'fecha_disp'=> '2020-06-05',
        'nro_disp'=>'11111',
        'resumen'=> 'Resumen Contrato1',
        'nombre_archivo'=> 'Arch. Contrato1',
        'tiene_anexo'=> 1,
        'estado_id'=> 1,
        'solicitante_id'=> 1,
        'objeto_id'=> 1,
        'persona_id'=> 1,
        ]);

        DB::table('contratos')->insert([
        'nro_contrato'=> '2/2020',
        'contrato'=> 'Contrato2',
        'fecha_desde'=> '2020-02-01',
        'fecha_hasta'=> '2020-03-03',
        'fecha_carga'=> '2020-02-01',
        'nro_disp'=>'11112',
        'resumen'=> 'Resumen Contrato2',
        'nombre_archivo'=> 'Arch. Contrato2',
        'tiene_anexo'=> 1,
        'estado_id'=> 1,
        'persona_id'=> 1,
        'objeto_id'=> 1,
        'solicitante_id'=> 1,
        ]);

        DB::table('contratos')->insert([
        'nro_contrato'=> '3/2020',
        'contrato'=> 'Contrato3',
        'fecha_desde'=> '2020-03-03',
        'fecha_hasta'=> '2020-04-05',
        'fecha_carga'=> '2020-03-03',
        'nro_disp'=>'11112',
        'resumen'=> 'Resumen Contrato3',
        'tiene_anexo'=> 0,
        'estado_id'=> 1,
        'persona_id'=> 1,
        'objeto_id'=> 1,
        'solicitante_id'=> 2,
        ]);

        DB::table('contratos')->insert([
        'nro_contrato'=> '4/2020',
        'contrato'=> 'Contrato4',
        'fecha_desde'=> '2020-04-03',
        'fecha_hasta'=> '2020-05-05',
        'nro_disp'=>'11112',
        'resumen'=> 'Resumen Contrato4',
        'nombre_archivo'=> 'Arch. Contrato4',
        'fecha_carga'=> '2020-04-02',
        'tiene_anexo'=> 0,
        'estado_id'=> 1,
        'persona_id'=> 2,
        'solicitante_id'=> 3,
        ]);

        DB::table('contratos')->insert([
        'nro_contrato'=> '5/2020',
        'contrato'=> 'Contrato5',
        'fecha_desde'=> '2020-05-03',
        'fecha_hasta'=> '2020-06-05',
        'nro_disp'=>'11112',
        'resumen'=> 'Resumen Contrato5',
        'nombre_archivo'=> 'Arch. Contrato5',
        'fecha_carga'=> '2020-02-02',
        'tiene_anexo'=> 1,
        'estado_id'=> 1,
        'persona_id'=> 2,
        'objeto_id'=> 1,
        'solicitante_id'=> 3,
        ]);

        DB::table('contratos')->insert([
        'nro_contrato'=> '6/2020',
        'contrato'=> 'Contrato6',
        'fecha_desde'=> '2020-09-03',
        'fecha_hasta'=> '2020-09-05',
        'nro_disp'=>'11113',
        'resumen'=> 'Resumen Contrato6',
        'nombre_archivo'=> 'Arch. Contrato6',
        'fecha_carga'=> '2020-01-02',
        'tiene_anexo'=> 1,
        'estado_id'=> 1,
        'persona_id'=> 2,
        'objeto_id'=> 1,
        'solicitante_id'=> 1,
        ]);

        DB::table('contratos')->insert([
        'nro_contrato'=> '7/2020',
        'contrato'=> 'Contrato7',
        'fecha_desde'=> '2020-10-03',
        'fecha_hasta'=> '2020-10-05',
        'nro_disp'=>'11112',
        'resumen'=> 'Resumen Contrato7',
        'nombre_archivo'=> 'Arch. Contrato7',
        'fecha_carga'=> '2020-05-02',
        'tiene_anexo'=> 0,
        'estado_id'=> 1,
        'persona_id'=> 1,
        'solicitante_id'=> 4,
        ]);

        DB::table('contratos')->insert([
        'nro_contrato'=> '8/2020',
        'contrato'=> 'Contrato8',
        'fecha_desde'=> '2020-07-03',
        'fecha_hasta'=> '2020-07-05',
        'nro_disp'=>'11112',
        'resumen'=> 'Resumen Contrato8',
        'nombre_archivo'=> 'Arch. Contrato8',
        'fecha_carga'=> '2020-06-02',
        'tiene_anexo'=> 0,
        'estado_id'=> 2,
        'persona_id'=> 1,
        'solicitante_id'=> 4,
        ]);

        DB::table('contratos')->insert([
        'nro_contrato'=> '9/2020',
        'contrato'=> 'Contrato9',
        'fecha_desde'=> '2020-11-03',
        'fecha_hasta'=> '2020-11-05',
        'nro_disp'=>'11212',
        'resumen'=> 'Resumen Contrato9',
        'nombre_archivo'=> 'Arch. Contrato9',
        'fecha_carga'=> '2020-04-02',
        'tiene_anexo'=> 1,
        'estado_id'=> 2,
        'persona_id'=> 3,
        ]);

        DB::table('contratos')->insert([
        'nro_contrato'=> '10/2020',
        'contrato'=> 'Contrato10',
        'fecha_desde'=> '2020-05-03',
        'fecha_hasta'=> '2020-06-05',
        'nro_disp'=>'11112',
        'resumen'=> 'Resumen Contrato4',
        'nombre_archivo'=> 'Arch. Contrato10',
        'fecha_carga'=> '2020-03-02',
        'tiene_anexo'=> 0,
        'estado_id'=> 1,
        'persona_id'=> 1,
        ]);

    }
}
