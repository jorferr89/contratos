<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        'name'=>'Administrador',
        'email'=> 'admin@mail.com',
        'password'=> bcrypt(12345678),
        'rol_id'=>1,
        ]);

        DB::table('users')->insert([
        'name'=>'Contrato',
        'email'=> 'contrato@mail.com',
        'password'=> bcrypt(12345678),
        'rol_id'=>2,
        ]);

        DB::table('users')->insert([
        'name'=>'Auditor',
        'email'=> 'audit@mail.com',
        'password'=> bcrypt(12345678),
        'rol_id'=>3,
        ]);

    }
}
