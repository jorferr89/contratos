<?php

use Illuminate\Database\Seeder;
use App\Estado;

class EstadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('estados')->insert([
        'nombre'=> 'Activo',
        ]);

        DB::table('estados')->insert([
        'nombre'=> 'Reservado',
        ]);

        DB::table('estados')->insert([
        'nombre'=> 'Baja',
        ]);

    }
}
