<?php

use Illuminate\Database\Seeder;
use App\Sexo;

class SexoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sexos')->insert([
        'nombre'=> 'Femenino',
        ]);

        DB::table('sexos')->insert([
        'nombre'=> 'Masculino',
        ]);

        DB::table('sexos')->insert([
        'nombre'=> 'No declara',
        ]);
    }
}
