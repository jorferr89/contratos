<?php

use Illuminate\Database\Seeder;
use App\Persona;

class PersonaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('personas')->insert([
        'apellidos'=> 'Rodriguez',
        'nombres'=> 'Jorge Fernando',
        'nro_documento'=> '33111444',
        'domicilio'=> 'San Jose',
        'mail'=> 'jorge@mail.com',
        'nro_documento'=> 33111444,
        'telefono'=> '3758613741',
        'fecha_nacimiento'=> '1989-09-11',
        'cuit'=> '20348953111',
        'sexo_id'=> 2,
        ]);

        DB::table('personas')->insert([
        'apellidos'=> 'Villalba',
        'nombres'=> 'Carlos',
        'nro_documento'=> 33111448,
        'domicilio'=> 'Posadas',
        'mail'=> 'carlos@mail.com',
        'nro_documento'=> '33111448',
        'telefono'=> '3758613744',
        'fecha_nacimiento'=> '1990-09-11',
        'cuit'=> '20348953119',
        'sexo_id'=> 2,
        ]);

        DB::table('personas')->insert([
        'apellidos'=> 'Pereyra',
        'nombres'=> 'Emiliano',
        'nro_documento'=> 36111448,
        'domicilio'=> 'Posadas',
        'mail'=> 'emiliano@mail.com',
        'nro_documento'=> '33181448',
        'telefono'=> '3758682744',
        'fecha_nacimiento'=> '1990-12-11',
        'cuit'=> '20368953119',
        'sexo_id'=> 2,
        ]);
    }
}
