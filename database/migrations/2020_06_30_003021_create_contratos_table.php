<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContratosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contratos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nro_contrato');
            $table->string('contrato');
            $table->date('fecha_desde');
            $table->date('fecha_hasta');
            $table->date('fecha_disp')->nullable();
            $table->string('nro_disp')->nullable();
            $table->string('resumen')->nullable();
            $table->string('nombre_archivo')->nullable();
            $table->date('fecha_carga');
            $table->boolean('tiene_anexo')->default(0);
            $table->unsignedBigInteger('estado_id');
            $table->unsignedBigInteger('solicitante_id')->nullable();
            $table->unsignedBigInteger('objeto_id')->nullable();
            $table->unsignedBigInteger('persona_id');
            $table->unsignedBigInteger('anexo_id')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contratos');
    }
}
