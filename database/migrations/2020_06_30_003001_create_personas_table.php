<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('apellidos');
            $table->string('nombres');
            $table->bigInteger('nro_documento');
            $table->string('domicilio')->nullable();
            $table->string('mail');
            $table->string('telefono');
            $table->date('fecha_nacimiento')->nullable();
            $table->string('cuit')->nullable();
            $table->unsignedBigInteger('sexo_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas');
    }
}
