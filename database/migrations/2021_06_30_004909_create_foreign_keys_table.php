<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('users', function (Blueprint $table) {
            $table->foreign('rol_id')->references('id')->on('roles');
        });

        Schema::table('personas', function (Blueprint $table) {
            $table->foreign('sexo_id')->references('id')->on('sexos');
        });

        Schema::table('contratos', function (Blueprint $table) {
            $table->foreign('estado_id')->references('id')->on('estados');
            $table->foreign('solicitante_id')->references('id')->on('solicitantes');
            $table->foreign('objeto_id')->references('id')->on('objetos');
            $table->foreign('persona_id')->references('id')->on('personas');
            $table->foreign('anexo_id')->references('id')->on('anexos');
        });

        Schema::table('anexos', function (Blueprint $table) {
            $table->foreign('contrato_id')->references('id')->on('contratos');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foreign_keys');
    }
}
