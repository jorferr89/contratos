<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth', 'administrador'])->group(function()
{


	Route::prefix('estados')->group(function() {
		Route::get('/', 'EstadoController@index')->name('estados');

		Route::get('crear', 'EstadoController@crear')->name('estados.crear');
		Route::post('crear', 'EstadoController@guardar')->name('estados.guardar');
		Route::get('editar/{estado}', 'EstadoController@editar')->name('estados.editar');
		Route::put('editar/{estado}', 'EstadoController@actualizar')->name('estados.actualizar');

		Route::delete('eliminar/{estado}', 'EstadoController@eliminar')->name('estados.eliminar');
	});

	Route::prefix('objetos')->group(function() {
		Route::get('/', 'ObjetoController@index')->name('objetos');

		Route::get('crear', 'ObjetoController@crear')->name('objetos.crear');
		Route::post('crear', 'ObjetoController@guardar')->name('objetos.guardar');

		Route::get('editar/{objeto}', 'ObjetoController@editar')->name('objetos.editar');
		Route::put('editar/{objeto}', 'ObjetoController@actualizar')->name('objetos.actualizar');

		Route::delete('eliminar/{objeto}', 'ObjetoController@eliminar')->name('objetos.eliminar');
	});

	Route::prefix('personas')->group(function() {
		Route::get('/', 'PersonaController@index')->name('personas');

		Route::get('crear', 'PersonaController@crear')->name('personas.crear');
		Route::post('crear', 'PersonaController@guardar')->name('personas.guardar');

		Route::get('editar/{persona}', 'PersonaController@editar')->name('personas.editar');
		Route::put('editar/{persona}', 'PersonaController@actualizar')->name('personas.actualizar');

		Route::delete('eliminar/{persona}', 'PersonaController@eliminar')->name('personas.eliminar');

	});

	Route::prefix('sexos')->group(function() {
		Route::get('/', 'SexoController@index')->name('sexos');

		Route::get('crear', 'SexoController@crear')->name('sexos.crear');
		Route::post('crear', 'SexoController@guardar')->name('sexos.guardar');
		Route::get('editar/{sexo}', 'SexoController@editar')->name('sexos.editar');
		Route::put('editar/{sexo}', 'SexoController@actualizar')->name('sexos.actualizar');

		Route::delete('eliminar/{sexo}', 'SexoController@eliminar')->name('sexos.eliminar');
	});

	Route::prefix('solicitantes')->group(function() {
		Route::get('/', 'SolicitanteController@index')->name('solicitantes');

		Route::get('crear', 'SolicitanteController@crear')->name('solicitantes.crear');
		Route::post('crear', 'SolicitanteController@guardar')->name('solicitantes.guardar');
	Route::get('editar/{solicitante}', 'SolicitanteController@editar')->name('solicitantes.editar');
		Route::put('editar/{solicitante}', 'SolicitanteController@actualizar')->name('solicitantes.actualizar');

		Route::delete('eliminar/{solicitante}', 'SolicitanteController@eliminar')->name('solicitantes.eliminar');
	});
});	

Route::middleware(['auth', 'contrato'])->group(function()
{

	Route::prefix('contratos')->group(function() {
	Route::match(['get', 'post'], '/', 'ContratoController@index')->name('contratos');
	//Route::get('/', 'ContratoController@index')->name('contratos');
	
	Route::get('crear', 'ContratoController@crear')->name('contratos.crear');
	Route::post('crear', 'ContratoController@guardar')->name('contratos.guardar');

	Route::get('editar/{contrato}', 'ContratoController@editar')->name('contratos.editar');
	Route::put('editar/{contrato}', 'ContratoController@actualizar')->name('contratos.actualizar');

	//Route::post('guardarAnexo/{contrato}', 'ContratoController@guardarAnexo')->name('contratos.guardarAnexo');
	Route::match(['get', 'post'], 'guardarAnexo/{contrato}', 'ContratoController@guardarAnexo')->name('contratos.guardarAnexo');

	//Route::match(['get', 'post'], '/buscar', 'ContratoController@buscar')->name('contratos.buscar');
	Route::get('buscar', 'ContratoController@buscar')->name('contratos.buscar');
	Route::post('buscar', 'ContratoController@filtrar');


	Route::delete('eliminar/{contrato}', 'ContratoController@eliminar')->name('contratos.eliminar');

	});


});

Route::middleware(['auth', 'auditor'])->group(function()
{

	Route::prefix('auditoria')->group(function() {
		Route::match(['get', 'post'], '/', 'AuditoriaController@index')->name('audits');
	
	});
	

});
