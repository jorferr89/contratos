<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Persona extends Model implements Auditable
{

    use \OwenIt\Auditing\Auditable;

    public $timestamps = false;

    protected $fillable = ['apellidos','nombres', 'nro_documento', 'domicilio', 'mail', 'telefono', 'fecha_nacimiento', 'cuit', 'sexo_id'];

    protected $table = 'personas';

    public function sexo() {
        return $this->belongsTo('App\Sexo');
    } 

    public function contratos(){
        return $this->hasMany('App\Contrato');
    } 

}
