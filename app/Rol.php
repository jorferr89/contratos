<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Rol extends Model implements Auditable
{

    use \OwenIt\Auditing\Auditable;

    public $timestamps = false;

    protected $fillable = ['nombre'];

    protected $table = 'roles';

    public function users(){
        return $this->hasMany('App\User');
    } 
}
