<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Contrato extends Model implements Auditable
{

    use \OwenIt\Auditing\Auditable;
    
    protected $fillable = ['nro_contrato','contrato', 'fecha_desde', 'fecha_hasta', 'fecha_disp', 'nro_disp', 'resumen', 'nombre_archivo', 'fecha_carga', 'anexo_id', 'estado_id', 'solicitante_id', 'objeto_id', 'persona_id', 'tiene_anexo'];

    protected $table = 'contratos';

    public $timestamps = false;

    public function anexo() {
        return $this->belongsTo('App\Anexo');
    } 

    public function estado() {
        return $this->belongsTo('App\Estado');
    } 

    public function solicitante() {
        return $this->belongsTo('App\Solicitante');
    } 

    public function objeto() {
        return $this->belongsTo('App\Objeto');
    } 

    public function persona() {
        return $this->belongsTo('App\Persona');
    } 
}
