<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Sexo extends Model implements Auditable
{

    use \OwenIt\Auditing\Auditable;

    public $timestamps = false;

    protected $fillable = ['nombre'];

    protected $table = 'sexos';

    //hasMany cuando el id lo tiene el otro
    //belongs si el id lo tengo yo
    public function personas(){
        return $this->hasMany('App\Persona');
    } 
}
