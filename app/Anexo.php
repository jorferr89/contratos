<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use OwenIt\Auditing\Contracts\Auditable;

class Anexo extends Model 
{

    //use \OwenIt\Auditing\Auditable;
    
    public $timestamps = false;

    protected $fillable = ['anexo', 'contrato_id'];

    protected $table = 'anexos';

    public function contrato() {
        return $this->belongsTo('App\Contrato');
    } 

}
