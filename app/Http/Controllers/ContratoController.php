<?php

namespace App\Http\Controllers;

use App\Contrato;

use App\Estado;
use App\Solicitante;
use App\Objeto;
use App\Persona;
use App\Anexo;
use App\Http\Requests\ContratoRequest;
use App\Http\Requests\ContratoEditarRequest;
use App\Http\Requests\AnexoRequest;


use Illuminate\Http\Request;

class ContratoController extends Controller
{
    public function index(Request $request) { // muestra todos los usuarios
        $fecha_desde=$request->fecha_desde;
        $fecha_hasta=$request->fecha_hasta;

        if($fecha_desde!=null && $fecha_hasta!=null){
            $request->validate([
                            'fecha_hasta'=>'after_or_equal:fecha_desde',
                            'fecha_desde'=>'before_or_equal:fecha_hasta'
                           ], [
                            'after_or_equal'=> 'La fecha hasta no puede ser anterior a la fecha desde',
                            'before_or_equal'=> 'La fecha desde no puede ser posterior a la fecha hasta',      
            ]); 
        }

        $consulta = Contrato::select('id', 'nro_contrato', 'contrato', 'nro_disp', 'fecha_desde', 'fecha_hasta', 'fecha_carga', 'estado_id', 'solicitante_id', 'objeto_id', 'persona_id', 'tiene_anexo');

        if($fecha_desde!=null)
            $consulta=$consulta->where('fecha_desde', '>=', $fecha_desde);

        if($fecha_hasta!=null)
            $consulta=$consulta->where('fecha_hasta', '<=', $fecha_hasta);

        $contratos=$consulta->orderBy('id', 'desc')->get();

        //dd($consulta);

        return view('contratos.index', compact('contratos', 'fecha_desde', 'fecha_hasta'));

    }

    public function crear() {
        $anioActual=now()->year;    
        $cantidad=Contrato::whereYear('fecha_carga', $anioActual)->count();
        $cant=$cantidad+1;
        $numero=$cant."/".$anioActual;
    	$estados=Estado::all();
        $solicitantes=Solicitante::all();
        $objetos=Objeto::all();
        $personas=Persona::all();
        return view('contratos.crear', compact('numero','estados', 'solicitantes', 'objetos', 'personas'));
    }

    public function guardar(ContratoRequest $request) {

        //$request->merge(['user_id'=>auth()->id()]);
        $anioActual=now()->year;
        
        $cantidad=Contrato::whereYear('fecha_carga', $anioActual)->count();
        $cant=$cantidad+1;
        $numero=$cant."/".$anioActual;
        

        $contrato=Contrato::create([
                            'nro_contrato'=>$numero,
                            'contrato'=>$request->contrato,
                            'fecha_desde'=>$request->fecha_desde,
                            'fecha_hasta'=>$request->fecha_hasta,
                            'fecha_disp'=>$request->fecha_disp,
                            'nro_disp'=>$request->nro_disp,
                            'resumen'=>$request->resumen,
                            'nombre_archivo'=>$request->nombre_archivo,
                            'fecha_carga'=>now(),
                            'tiene_anexo'=>0,
                            'estado_id'=>$request->estado_id,
                            'solicitante_id'=>$request->solicitante_id,
                            'objeto_id'=>$request->objeto_id,
                            'persona_id'=>$request->persona_id,
                        ]);

        return redirect()->route('contratos')->withMessage('Contrato creado');
    }

    public function editar(Contrato $contrato) {
        $estados=Estado::all();
        $solicitantes=Solicitante::all();
        $objetos=Objeto::all();
        $personas=Persona::all();
        return view('contratos.editar',compact('contrato', 'estados', 'solicitantes', 'objetos', 'personas'));
    }

    public function actualizar(ContratoEditarRequest $request, Contrato $contrato) {
        $contrato->update([
            "contrato"       => $request->contrato,
            "fecha_desde"       => $request->fecha_desde,
            "fecha_hasta"       => $request->fecha_hasta,
            "fecha_disp"       => $request->fecha_disp,
            "nro_disp"       => $request->nro_disp,
            "resumen"       => $request->resumen,
            "nombre_archivo"       => $request->nombre_archivo,
            "estado_id"       => $request->estado_id,
            "solicitante_id"       => $request->solicitante_id,
            "objeto_id"       => $request->objeto_id,
            "persona_id"       => $request->persona_id,
        ]);
        return redirect()->route('contratos')->withMessage('Contrato actualizado');
    }


    public function guardarAnexo(Contrato $contrato, AnexoRequest $request) {

        //$contrato=$contrato->id;
        //dd($contrato);

        $anexo=Anexo::create([
                            'anexo'=>$request->anexo,
                            'contrato_id'=>$contrato->id,
                        ]);

        //dd($anexo);

        $contrato->update([
            "tiene_anexo"       => 1,
            "anexo_id" => $anexo->id,
        ]);

        return redirect()->route('contratos')->withMessage('Anexo agregado al Contrato');
    }

    public function buscar() {

        
        $estados=Estado::all();
        $estado=null;
        $solicitantes=Solicitante::all();
        $solicitante=null;
        $objetos=Objeto::all();
        $objeto=null;
        $personas=Persona::all();
        $persona=null;
        $fecha_desde=null;
        $fecha_hasta=null;
        $resultado= null;
        return view('contratos.buscar', compact('estados', 'fecha_desde', 'fecha_hasta', 'resultado','estado', 'solicitante', 'solicitantes', 'objetos', 'objeto', 'personas', 'persona'));

    }

    public function filtrar(Request $request){
        $request->validate([
                            'fecha_desde'=>'nullable|date',
                            'fecha_hasta'=>'nullable|date'
                           ]);

        if($request->fecha_desde && $request->fecha_hasta)
            $request->validate([
                            'fecha_hasta'=>'after_or_equal:fecha_desde',
                            'fecha_desde'=>'before_or_equal:fecha_hasta'
                           ], [
                            'after_or_equal'=> 'La fecha hasta no puede ser anterior a la fecha desde',
                            'before_or_equal'=> 'La fecha desde no puede ser posterior a la fecha hasta',      
            ]); 
        
        $fecha_desde=$request->fecha_desde;
        $fecha_hasta=$request->fecha_hasta;
        
        $estados=Estado::all();
        $solicitantes=Solicitante::all();
        $objetos=Objeto::all();
        $personas=Persona::all();
            

        $consulta= Contrato::orderBy('nro_contrato', 'asc');

        if($request->estado_id!=null){
            $consulta=$consulta->whereEstado_id($request->estado_id);
            $estado=Estado::find($request->estado_id);
        }

        else 
            $estado=null;

        if($request->solicitante_id!=null){
            $consulta=$consulta->whereSolicitante_id($request->solicitante_id);
            $solicitante=Solicitante::find($request->solicitante_id);
        }

        else 
            $solicitante=null;

        if($request->objeto_id!=null){
            $consulta=$consulta->whereObjeto_id($request->objeto_id);
            $objeto=Objeto::find($request->objeto_id);
        }

        else 
            $objeto=null;

        if($request->persona_id!=null){
            $consulta=$consulta->wherePersona_id($request->persona_id);
            $persona=Persona::find($request->persona_id);
        }

        else 
            $persona=null;
        
        $fecha_desde=$request->fecha_desde;
        $fecha_hasta=$request->fecha_hasta;

        if($fecha_desde!=null)
            $consulta=$consulta->where('fecha_desde', '>=', $fecha_desde);

        if($fecha_hasta!=null)                
            $consulta=$consulta->where('fecha_hasta', '<=', $fecha_hasta);

        $filtros=$consulta->pluck('id');


        $resultado=Contrato::select('contratos.*')
                                    ->whereIn('contratos.id', $filtros)
                                    ->orderBy('id', 'desc')
                                    ->get();
        //dd($resultado);

        return view('contratos.buscar', compact('estados', 'fecha_desde', 'fecha_hasta', 'resultado','estado', 'solicitante', 'solicitantes', 'objeto', 'objetos', 'persona', 'personas'));
    }



    public function eliminar(Contrato $contrato) {

        $references = Anexo::whereContrato_id($contrato->id)->count();

        //dd($references);

        if ($references > 0){ 
            return redirect()->route('contratos')->withErrors('No se puede eliminar el Contrato porque hay registros asociados');
        }
        

        $contrato->delete();
        return redirect()->route('contratos')->withMessage('Contrato eliminado');
    }




}
