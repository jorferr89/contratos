<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Objeto;
use App\Contrato;
use App\Http\Requests\ObjetoRequest;

class ObjetoController extends Controller
{
    public function index() { // muestra todos los usuarios
    	$objetos=Objeto::orderBy('descripcion', 'asc')->get();
    	return view('objetos.index', compact('objetos'));
    }

    public function crear() {
        return view('objetos.crear');
    }

    public function guardar(ObjetoRequest $request) {

        //$request->merge(['user_id'=>auth()->id()]);

        Objeto::create([
                            'descripcion'=>$request->descripcion,
                        ]);

        return redirect()->route('objetos')->withMessage('Objeto creado');
    }

    public function editar(Objeto $objeto) {
        return view('objetos.editar', compact('objeto'));
    }

    public function actualizar(ObjetoRequest $request, Objeto $objeto) {
        $objeto->update($request->all());
        return redirect()->route('objetos')->withMessage('Objeto actualizado');
    }

    public function eliminar(Objeto $objeto) {

        $references = Contrato::whereObjeto_id($objeto->id)->count();

        //dd($references);

        if ($references > 0){ 
            return redirect()->route('objetos')->withErrors('No se puede eliminar el Objeto porque hay registros asociados');
        }
        

        $objeto->delete();
        return redirect()->route('objetos')->withMessage('Objeto eliminado');
    }
}
