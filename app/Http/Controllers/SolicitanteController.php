<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Solicitante;
use App\Contrato;
use App\Http\Requests\SolicitanteRequest;

class SolicitanteController extends Controller
{
    public function index() { // muestra todos los usuarios
    	$solicitantes=Solicitante::orderBy('nombre', 'asc')->get();
    	return view('solicitantes.index', compact('solicitantes'));
    }

    public function crear() {
        return view('solicitantes.crear');
    }

    public function guardar(SolicitanteRequest $request) {

        //$request->merge(['user_id'=>auth()->id()]);

        Solicitante::create([
                            'nombre'=>$request->nombre,
                        ]);

        return redirect()->route('solicitantes')->withMessage('Solicitante creado');
    }

    public function editar(Solicitante $solicitante) {
        return view('solicitantes.editar', compact('solicitante'));
    }

    public function actualizar(SolicitanteRequest $request, Solicitante $solicitante) {
        $solicitante->update($request->all());
        return redirect()->route('solicitantes')->withMessage('Solicitante actualizado');
    }

    public function eliminar(Solicitante $solicitante) {
        $references = Contrato::whereSolicitante_id($solicitante->id)->count();

        //dd($references);

        if ($references > 0){ 
            return redirect()->route('solicitantes')->withErrors('No se puede eliminar al Solicitante porque hay registros asociados');
        }
        $solicitante->delete();
        return redirect()->route('solicitantes')->withMessage('Solicitante eliminado');
    }

}
