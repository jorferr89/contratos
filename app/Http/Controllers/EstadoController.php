<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Estado;
use App\Contrato;
use App\Http\Requests\EstadoRequest;

class EstadoController extends Controller
{
    public function index() { // muestra todos los usuarios
    	$estados=Estado::orderBy('nombre', 'asc')->get();
    	return view('estados.index', compact('estados'));
    }

    public function crear() {
        return view('estados.crear');
    }

    public function guardar(EstadoRequest $request) {

        //$request->merge(['user_id'=>auth()->id()]);

        Estado::create([
                            'nombre'=>$request->nombre,
                        ]);

        return redirect()->route('estados')->withMessage('Estado creado');
    }

    public function editar(Estado $estado) {
        return view('estados.editar', compact('estado'));
    }

    public function actualizar(EstadoRequest $request, Estado $estado) {
        $estado->update($request->all());
        return redirect()->route('estados')->withMessage('Estado actualizado');
    }

    public function eliminar(Estado $estado) {

        $references = Contrato::whereEstado_id($estado->id)->count();

        //dd($references);

        if ($references > 0){ 
            return redirect()->route('estados')->withErrors('No se puede eliminar el Estado porque hay registros asociados');
        }
        $estado->delete();
        return redirect()->route('estados')->withMessage('Estado eliminado');
    }


}
