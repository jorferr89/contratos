<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sexo;
use App\Persona;
use App\Http\Requests\SexoRequest;

class SexoController extends Controller
{
    public function index() { // muestra todos los usuarios
    	$sexos=Sexo::orderBy('nombre', 'asc')->get();
    	return view('sexos.index', compact('sexos'));
    }

    public function crear() {
        return view('sexos.crear');
    }

    public function guardar(SexoRequest $request) {

        //$request->merge(['user_id'=>auth()->id()]);

        Sexo::create([
                            'nombre'=>$request->nombre,
                        ]);

        return redirect()->route('sexos')->withMessage('Sexo creado');
    }

    public function editar(Sexo $sexo) {
        return view('sexos.editar', compact('sexo'));
    }

    public function actualizar(SexoRequest $request, Sexo $sexo) {
        $sexo->update($request->all());
        return redirect()->route('sexos')->withMessage('Sexo actualizado');
    }

    public function eliminar(Sexo $sexo) {

        $references = Persona::whereSexo_id($sexo->id)->count();

        //dd($references);

        if ($references > 0){ 
            return redirect()->route('sexos')->withErrors('No se puede eliminar el Sexo porque hay registros asociados');
        }
        $sexo->delete();
        return redirect()->route('sexos')->withMessage('Sexo eliminado');
    }

}
