<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Persona;
use App\Sexo;
use App\Contrato;
use App\Http\Requests\PersonaRequest;
use App\Http\Requests\PersonaEditarRequest;


class PersonaController extends Controller
{
    public function index() { // muestra todos los usuarios
    	$personas=Persona::orderBy('apellidos', 'asc')->get();
    	return view('personas.index', compact('personas'));
    }

    public function crear() {
        $sexos=Sexo::all();
        return view('personas.crear', compact('sexos'));
    }

    public function guardar(PersonaRequest $request) {

        //$request->merge(['user_id'=>auth()->id()]);

        Persona::create([
                            'apellidos'=>$request->apellidos,
                            'nombres'=>$request->nombres,
                            'nro_documento'=>$request->nro_documento,
                            'domicilio'=>$request->domicilio,
                            'mail'=>$request->mail,
                            'telefono'=>$request->telefono,
                            'fecha_nacimiento'=>$request->fecha_nacimiento,
                            'cuit'=>$request->cuit,
                            'sexo_id'=>$request->sexo_id,
                        ]);

        return redirect()->route('personas')->withMessage('Persona creada');
    }

    public function editar(Persona $persona) {
        $sexos=Sexo::all();
        return view('personas.editar', compact('persona', 'sexos'));
    }

    public function actualizar(PersonaEditarRequest $request, Persona $persona) {
        $persona->update($request->all());
        return redirect()->route('personas')->withMessage('Persona actualizada');
    }

    public function eliminar(Persona $persona) {

        $references = Contrato::wherePersona_id($persona->id)->count();

        //dd($references);

        if ($references > 0){ 
            return redirect()->route('personas')->withErrors('No se puede eliminar la Persona porque hay registros asociados');
        }
        

        $persona->delete();
        return redirect()->route('personas')->with('message','Persona eliminada');
    }
}
