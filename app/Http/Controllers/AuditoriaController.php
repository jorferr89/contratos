<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuditoriaController extends Controller
{
    public function index(Request $request)
    {
        $fecha_desde=$request->fecha_desde;
        $fecha_hasta=$request->fecha_hasta;
        $auditable_type=$request->auditable_type;
        $auditable_id=$request->auditable_id;

       

        $path = app_path();
        $types = [];
        $results = scandir($path);
        foreach ($results as $result) 
        {
            if ($result === '.' or $result === '..') continue;
                    $filename = $path . '/' . $result;
            if (!is_dir($filename)) 
                {   

                    $types[] = substr($result,0,-4);
                }
        }

        //$types = DB::table('audits')->distinct()->pluck("auditable_type");
      
        //dd($types);
        if(count($request->all())>1)
        {
            $sql = \OwenIt\Auditing\Models\Audit::select('audits.*');

            if($fecha_desde)
            {
                $sql = $sql->whereDate('created_at','>=',$fecha_desde);
            }            
            if($fecha_hasta)
            {
                $sql = $sql->whereDate('created_at','<=',$fecha_hasta);
            }
            if($auditable_type)
            {
                $sql = $sql->whereAuditable_type('App\\' . $auditable_type);
            }    
            if($auditable_id)
            {
                $sql = $sql->whereAuditable_id($auditable_id);
            }          
        
            $audits=$sql->orderBy('created_at','desc')->get();
        }
        else 
        {
            //            $audits =  \OwenIt\Auditing\Models\Audit::with('user')

            $audits =  \OwenIt\Auditing\Models\Audit::select('audits.*')
            ->orderBy('created_at','desc')
            ->get();    
        }
      

        return view('audits.index',[
            "audits"        =>  $audits,
            "fecha_desde"         =>  $fecha_desde,
            "fecha_hasta"         =>  $fecha_hasta,
            "auditable_type"=>  $auditable_type,
            "types"         =>  $types,
            "auditable_id"  =>  $auditable_id,

            ]);
        
    }

}
