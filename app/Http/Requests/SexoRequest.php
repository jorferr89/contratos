<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SexoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|max:30|unique:sexos',
        ];
    }

    public function messages() {
        return [
            'required' => 'Campo requerido',
            'unique' => 'El Nombre ya existe',
            'max' => 'Se permiten hasta 30 caracteres',
        ];

    }
}
