<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContratoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'nro_contrato' => 'required|numeric|unique:contratos',
            'contrato'=>'required|max:30',
            'fecha_desde'=>'required|date',
            'fecha_hasta'=>'required|date|after:fecha_desde',
            'fecha_disp'=>'nullable|date',
            'nro_disp' => 'nullable|numeric',
            'estado_id'=>'required',
            'persona_id'=>'required',
        ];
    }

    public function messages() {
        return [
            'required' => 'Campo requerido',
            //'nro_contrato.max' => 'Se permiten hasta 10 caracteres',
            'contrato.max' => 'Se permiten hasta 30 caracteres',
            'date'=> 'El campo acepta valores de fecha',
            'fecha_hasta.after' => 'La fecha hasta debe ser posterior a la fecha desde',
            'fecha_disp.after' => 'La fecha de disposición debe ser posterior a la fecha hasta',
            //'nro_contrato.unique' => 'El Nro. de Contrato ya existe',
            //'numeric' => 'Se permiten valores numéricos',
        ];

    }

}
