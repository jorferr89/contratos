<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AnexoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "anexo" => "required|mimes:pdf|max:10000",
            //"contrato_id" => "required",
        ];
    }

     public function messages() {
        return [
            'required' => 'Campo requerido',
            'max' => 'El archivo excede al tamaño permitido',
            'mimes' => 'Se aceptan solamente archivos PDF',
        ];

    }
}
