<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PersonaEditarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'apellidos' => 'required|max:100',
            'nombres'=>'required|max:100',
            //'nro_documento'=>'required|unique:personas',
            'mail'=>'required|email',
            'telefono'=>'required',
            'sexo_id'=>'required',
            'cuit'=>['nullable', 'regex:/\b(20|23|24|27|30|33|34)(\D)?[0-9]{8}(\D)?[0-9]/'],
        ];
    }

    public function messages() {
        return [
            'required' => 'Campo requerido',
            //'unique' => 'El Nro. de Documento ya existe',
            'apellidos.max' => 'Se permiten hasta 30 caracteres',
            'nombres.max' => 'Se permiten hasta 30 caracteres',
            'email'=> 'Mail No Válido (ejemplo@mail.com)',
            'nro_documento.max' => 'Se permiten solamente 8 caracteres',
            'nro_documento.min' => 'Se permiten solamente 8 caracteres',
            'cuit.regex'=>'Cuit No Válido',
        ];

    }
}
