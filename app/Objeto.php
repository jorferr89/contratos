<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Objeto extends Model implements Auditable
{

    use \OwenIt\Auditing\Auditable;

    public $timestamps = false;

    protected $fillable = ['descripcion'];

    protected $table = 'objetos';

    public function contratos(){
        return $this->hasMany('App\Contrato');
    } 
}
